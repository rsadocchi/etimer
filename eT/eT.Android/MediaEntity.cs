﻿using System.Threading.Tasks;
using Android.Media;
using eT.DependenciesInjectionInterfaces;

namespace eT.Droid
{
    public class MediaEntity : MediaPlayer, IAggregateSound
    {
        public async Task PlayAsync()
        {
            base.Start();
            await Task.CompletedTask;
        }
    }
}