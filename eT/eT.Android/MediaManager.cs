﻿using Android.Media;
using eT.DependenciesInjectionInterfaces;
using eT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(eT.Droid.MediaManager))]
namespace eT.Droid
{
    public class MediaManager : ISoundManager<MediaEntity>
    {
        public Dictionary<SoundFxEnum, MediaEntity> Effects;

        public MediaManager()
        {
            Effects = new Dictionary<SoundFxEnum, MediaEntity>();
        }

        public async Task<MediaEntity> LoadEffectAsync(string fxName, string ext = "mp3")
        {
            var me = new MediaEntity();
            var fd = Android.App.Application.Context.Assets.OpenFd($"{fxName}.{ext}");
            await me.SetDataSourceAsync(fd);
            me.Prepare();
            return await Task.FromResult(me);
        }

        public async Task LoadEffectsAsync()
        {
            Enum.GetValues(typeof(SoundFxEnum))
                .Cast<SoundFxEnum>()
                .ToList()
                .ForEach(async fx => { Effects.Add(fx, await LoadEffectAsync($"{fx.ToString()}")); });
            await Task.CompletedTask;
        }

        public async Task PlayAsync(SoundFxEnum fx)
        {
            var player = Effects
                .Where(o => o.Key == fx)
                .Select(o => o.Value)
                .FirstOrDefault();
            if (player != null)
                await player.PlayAsync();
            await Task.CompletedTask;
        }
    }
}