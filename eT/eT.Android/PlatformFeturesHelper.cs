﻿using Android.Content;
using Android.Views;
using eT.DependenciesInjectionInterfaces;
using eT.Models;
using Java.Interop;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(eT.Droid.PlatformFeturesHelper))]
namespace eT.Droid
{
    public class PlatformFeturesHelper : IPlatformFeaturesHelper
    {

        public async Task<DeviceOrientationEnum> GetDeviceOrientationAsync()
        {
            IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            var orientation = windowManager.DefaultDisplay.Rotation;
            return await Task.FromResult(
                (orientation == SurfaceOrientation.Rotation90 || orientation == SurfaceOrientation.Rotation270) ? DeviceOrientationEnum.Landscape :
                (orientation == SurfaceOrientation.Rotation0 || orientation == SurfaceOrientation.Rotation180) ? DeviceOrientationEnum.Portrait : 
                DeviceOrientationEnum.Undefined);
        }

        public async Task<string> GetImagesBasePathAsync() { return await Task.FromResult(string.Empty); }

        public async Task<string> GetLocalDbFullPathAsync(string dbName)
        {
            var folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Databases");
            if (!Directory.Exists(folder))
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    throw new Exception("You do not have permission for create new files.", ex);
                }

            var dbpath = Path.Combine(folder, dbName);

            if (!File.Exists(dbpath))
                using (var stream = File.Create(dbpath))
                    return await Task.FromResult(dbpath);

            return await Task.FromResult(dbpath);
        }
    }
}