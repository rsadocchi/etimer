﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace eT.Views
{
    public class Home : TabbedPage
    {
        public Home()
        {
            Children.Add(new SetupMMA() { Title = "MMA" });
            Children.Add(new SetupPankration() { Title = "Pankration" });
            Children.Add(new SetupTraining() { Title = "Training" });
            Children.Add(new SetupApp());

            Title = Children.First().Title;
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? "eTimer";
        }
    }
}
