﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eT.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SetupMMA : ContentPage
	{
		public SetupMMA ()
		{
			InitializeComponent ();
		}

        private void UIStepRoundNr_ValueChanged(object sender, ValueChangedEventArgs e)
        {

        }

        private void UIStepRoundTime_ValueChanged(object sender, ValueChangedEventArgs e)
        {

        }

        private void SwitchCell_OnChanged(object sender, ToggledEventArgs e)
        {

        }

        private void ButtonSave_Clicked(object sender, EventArgs e)
        {

        }

        private void ButtonReady_Clicked(object sender, EventArgs e)
        {

        }
    }
}