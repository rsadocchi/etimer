﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eT.Models
{
    public interface IAggregateTimerViewModel
    {
        int GetTotalReps();
        int GetRemainingReps();
        int GetWorkForRep();
        int GetRest();
    }

    public class MmaSetupViewModel : IAggregateTimerViewModel
    {
        public int NumberOfRounds { get; set; } = 5;
        public int TimeForRound { get; set; } = 180;
        public int Rest { get; set; } = 60;
        public bool Loop { get; set; } = false;
        public string Name { get; set; } = "";
        public int ActualRound { get; set; } = 0;

        public int GetRemainingReps() => (NumberOfRounds - ActualRound);
        public int GetRest() => Rest;
        public int GetTotalReps() => NumberOfRounds;
        public int GetWorkForRep() => TimeForRound;
    }
}
