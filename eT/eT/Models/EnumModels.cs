﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eT.Models
{
    public enum DeviceOrientationEnum
    {
        Undefined = -1,
        Portrait = 0,
        Landscape = 1
    }

    public enum SoundFxEnum
    {
        AirHorn,
        Buzzer,
        Click,
        GlassPing,
        GongMetal,
        Wood
    }
}
