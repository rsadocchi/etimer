﻿using eT.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eT.DependenciesInjectionInterfaces
{
    public interface IPlatformFeaturesHelper
    {
        Task<string> GetLocalDbFullPathAsync(string dbName);
        Task<string> GetImagesBasePathAsync();
        Task<DeviceOrientationEnum> GetDeviceOrientationAsync();
    }

    public interface IAggregateSound
    {
        Task PlayAsync();
    }

    public interface ISoundManager<TSound> where TSound : class , IAggregateSound
    {
        Task LoadEffectsAsync();

        Task<TSound> LoadEffectAsync(string fxName, string ext = "mp3");

        Task PlayAsync(SoundFxEnum fx);
    }
}
