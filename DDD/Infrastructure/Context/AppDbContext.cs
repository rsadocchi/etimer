﻿using DDD.Domain.AggregateModels;
using DDD.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DDD.Infrastructure.Context
{
    public class AppDbContext : DbContext, IUnitOfWork
    {
        public DbSet<MMA_Master> MMA_Masters { get; set; }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="options">class options (connection string .....)</param>
        public AppDbContext(DbContextOptions options) : base(options) { }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken)) { return (await base.SaveChangesAsync() > 0); }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
