﻿using DDD.Domain.SeedWork;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Domain.AggregateModels
{
    [Table(nameof(MMA_Master))]
    public class MMA_Master : Entity, IAggregateRoot
    {
        [PrimaryKey, AutoIncrement, NotNull]
        public int MMA_ID { get; set; }

        [Unique, MaxLength(50), NotNull]
        public string MMA_Name { get; set; }
    }
}
